<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-09 16:13:48
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-12 16:04:33
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : Brand.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
namespace app\mall\controller;

use app\mall\model\MallBrand;
use think\admin\helper\QueryHelper;
use think\admin\Controller;

/**
 * 商品品牌管理
 * Class Brand
 * @package app\mall\controller
 */
class Brand extends Controller
{
    /**
     * 商品品牌列表
     * @auth true
     * @menu true
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index()
    {
        MallBrand::mQuery()->layTable(function () {
            $this->title = "商品品牌管理";
        }, function (QueryHelper $query) {
            $query->like('brand_name_zh')->equal('status')->dateBetween('create_at');
        });
    }
    
    /**
     * 添加商品品牌
     * @auth true
     */
    public function add()
    {
        MallBrand::mForm('form');
    }

    /**
     * 编辑商品品牌
     * @auth true
     */
    public function edit()
    {
        MallBrand::mForm('form');
    }
    
    /**
     * 表单数据处理
     * @param array $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function _form_filter(array &$data)
    {
        if ($this->request->isPost()) {
            // 检查品牌名称是否出现重复
            if (!isset($data['id'])) {
                $where = ['brand_name_zh' => $data['brand_name_zh'], 'brand_name_en' => $data['brand_name_en']];
                if (MallBrand::mk()->where($where)->count() > 0) {
                    $this->error("品牌名称 {$data['brand_name_zh']}/{$data['brand_name_en']} 已经存在，请使用其它品牌名称！");
                }
            }
        }
    }

    /**
     * 修改商品品牌状态
     * @auth true
     */
    public function state()
    {
        MallBrand::mSave($this->_vali([
            'status.in:0,1'  => '状态值范围异常！',
            'status.require' => '状态值不能为空！',
        ]));
    }

    /**
     * 删除商品品牌
     * @auth true
     */
    public function remove()
    {
        MallBrand::mDelete();
    }
}