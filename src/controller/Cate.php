<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-09 17:51:44
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-19 18:35:57
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : Cate.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
namespace app\mall\controller;

use app\mall\model\MallCate;
use app\mall\model\MallType;
use think\admin\extend\DataExtend;
use think\admin\helper\QueryHelper;
use think\admin\Controller;

/**
 * 商品分类管理
 * Class Cate
 * @package app\mall\controller
 */
class Cate extends Controller
{
    /**
     * 最大级别
     * @var integer
     */
    protected $maxLevel = 3;
    
    /**
     * 商品分类列表
     * @auth true
     * @menu true
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index()
    {
        MallCate::mQuery()->layTable(function () {
            $this->title = "商品分类管理";
        }, function (QueryHelper $query) {
            $query->where(['deleted' => 0]);
            $query->like('name')->equal('status')->dateBetween('create_at');
        });
    }
    
    /**
     * 列表数据处理
     * @param array $data
     */
    protected function _index_page_filter(array &$data)
    {
        $data = DataExtend::arr2table($data);
    }

    /**
     * 绑定分类类目
     * @auth true
     */
    public function bind()
    {
        MallCate::mForm('bind');
    }
    
    /**
     * 添加商品分类
     * @auth true
     */
    public function add()
    {
        MallCate::mForm('form');
    }

    /**
     * 编辑商品分类
     * @auth true
     */
    public function edit()
    {
        MallCate::mForm('form');
    }

    /**
     * 表单数据处理
     * @param array $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function _form_filter(array &$data)
    {
        if ($this->request->isGet()) {
            $data['pid'] = intval($data['pid'] ?? input('pid', '0'));
            $this->cates = MallCate::getParentData($this->maxLevel, $data, [
                'id' => '0', 'pid' => '-1', 'name' => '顶部分类',
            ]);
            $this->types = MallType::mk()->where(['status'=>'1'])->column('id, type_name');
        } elseif($this->request->isPost()) {
            // 检查分类名称是否出现重复
            if (!isset($data['id'])) {
                $where = ['name' => $data['name'], 'pid' => $data['pid']];
                if (MallCate::mk()->where($where)->count() > 0) {
                    $this->error("分类 {$data['name']} 已经存在，请使用其它分类名称！");
                }
            }
        }
    }

    /**
     * 修改商品分类状态
     * @auth true
     */
    public function state()
    {
        MallCate::mSave($this->_vali([
            'status.in:0,1'  => '状态值范围异常！',
            'status.require' => '状态值不能为空！',
        ]));
    }
    
    /**
     * 删除商品分类
     * @auth true
     */
    public function remove()
    {
        MallCate::mDelete();
    }
}