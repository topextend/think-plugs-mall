<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-13 18:39:55
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-29 17:37:25
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : Goods.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
namespace app\mall\controller;

use app\mall\model\MallAttr;
use app\mall\model\MallCate;
use app\mall\model\MallGoods;
use think\admin\helper\QueryHelper;
use think\admin\Controller;

/**
 * 商品数据管理
 * Class Goods
 * @package app\mall\controller
 */
class Goods extends Controller
{
    /**
     * 商品数据管理
     * @auth true
     * @menu true
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index()
    {
        MallGoods::mQuery()->layTable(function () {
            $this->title = "商品数据管理";
        }, function (QueryHelper $query) {
            $query->like('goods_sn')->equal('status')->dateBetween('create_at');
        });
    }
    
    /**
     * 选择分类
     * @auth true
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function select()
    {
        $this->title = '确认商品类目';
        $this->goods_id = $this->app->request->get('goods_id','0');
        $this->cates = MallCate::getSelectCates();
        $this->fetch();
    }
    
    /**
     * 添加商品信息
     * @auth true
     */
    public function add()
    {
        $this->mode = 'add';
        $this->title = '创建商品信息';
        $this->cat_id = $this->app->request->get('cat_id');
        MallGoods::mForm('form');   
    }

    /**
     * 编辑商品信息
     * @auth true
     */
    public function edit()
    {
        $this->title = '编辑商品信息';
        $this->goods_id = $this->app->request->get('goods_id');
        $this->cat_id = $this->app->request->get('cat_id');
        MallGoods::mForm('form');
    }

    /**
     * 表单数据处理
     * @param array $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function _add_form_filter(array &$data)
    {
        //
    }
    
    /**
     * 表单数据处理
     * @param array $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function _form_filter(array &$data)
    {
        if ($this->request->isGet()) {
            $this->cats_path = MallCate::getSelectedCatPath($this->app->request->get('cat_id'));
            //获取分类绑定的类型ID
            $this->type_id   = MallCate::mk()->where(['id'=>$this->app->request->get('cat_id')])->value('type_id');
            if (empty($this->type_id)) $this->error("该分类未绑定类型属性，请先绑定类型属性！");
            if (empty(MallAttr::mk()->where(['type_id' => $this->type_id])->count())) $this->error("类型属性未建立，请先建立类型属性！");
            //获取属性数据
            $this->attrs = MallAttr::getAttrList($this->type_id);
        } elseif ($this->request->isPost()) {
            // 检查类型名称是否出现重复
            if (!isset($data['id'])) {
                $where = ['type_name' => $data['type_name']];
                if (MallType::mk()->where($where)->count() > 0) {
                    $this->error("类型名称 {$data['type_name']} 已经存在，请使用其它类型名称！");
                }
            }
        }
    }

    /**
     * 修改商品状态
     * @auth true
     */
    public function state()
    {
        MallGoods::mSave($this->_vali([
            'status.in:0,1'  => '状态值范围异常！',
            'status.require' => '状态值不能为空！',
        ]));
    }

    /**
     * 删除商品信息
     * @auth true
     */
    public function remove()
    {
        MallGoods::mDelete();
    }
}